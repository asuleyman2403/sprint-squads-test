import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  BASE_URL = '';

  constructor(private http: HttpClient) {
    this.BASE_URL = environment.BASE_URL;
  }

  searchUsers(query: string): Observable<any> {
    return this.http.get(
      `${this.BASE_URL}/search/users?q=${query}&per_page=20`
    );
  }

  getUsers(): Observable<any> {
    return this.http.get(`${this.BASE_URL}/users`);
  }

  getExactUser(user: string): Observable<any> {
    return this.http.get(`${this.BASE_URL}/users/${user}`);
  }

  getUserRepositories(user: string) {
    return this.http.get(`${this.BASE_URL}/users/${user}/repos`);
  }
}
