import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private filteredUsersSource = new BehaviorSubject<any[]>([]);
  private currentUserSource = new BehaviorSubject<any>(null);
  private repositoriesSource = new BehaviorSubject<any[]>([]);
  filteredUsers$ = this.filteredUsersSource.asObservable();
  currentUser$ = this.currentUserSource.asObservable();
  repositories$ = this.repositoriesSource.asObservable();
  constructor() {}

  updateFilteredUsersSource(users: any[]) {
    this.filteredUsersSource.next(users);
  }

  updateCurrentUser(user: any) {
    this.currentUserSource.next(user);
  }

  updateRepositories(repositories: any[]) {
    this.repositoriesSource.next(repositories);
  }
}
