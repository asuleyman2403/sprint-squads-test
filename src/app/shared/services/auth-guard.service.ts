import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate, CanLoad {
  constructor(private router: Router) {}

  canActivate(): boolean {
    return this.loggedIn();
  }

  canLoad(): boolean {
    return this.loggedIn();
  }

  loggedIn(): boolean {
    const isLoggedIn = localStorage.getItem('loggedIn');
    if (isLoggedIn) {
      return true;
    }

    this.router.navigate(['/login']);

    return false;
  }
}
