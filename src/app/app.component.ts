import { Component, OnInit } from '@angular/core';
import { users } from 'src/system-users';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'sprint-test';
  isModalVisible = false;
  ngOnInit() {
    console.log(users);
    localStorage.setItem('users', JSON.stringify(users));
    const visited = localStorage.getItem('visited');
    if (!visited) {
      this.isModalVisible = true;
    }
  }

  closeModal() {
    this.isModalVisible = false;
    localStorage.setItem('visited', 'true');
  }
}
