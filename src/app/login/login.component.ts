import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService
  ) {
    this.form = this.formBuilder.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  get login() {
    return this.form.get('login');
  }

  get password() {
    return this.form.get('password');
  }

  ngOnInit(): void {}

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    const { login, password } = this.form.value;
    const users = JSON.parse(localStorage.getItem('users') as any) as any[];
    const user = users.find((u) => u.login === login);
    if (user) {
      if (user.password === password) {
        localStorage.setItem('loggedIn', 'true');
        this.router.navigate(['/users']);
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Authentication error',
          detail: 'Wrong password!',
        });
      }
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'Authentication error',
        detail: 'Such user does not exist!',
      });
    }
  }
}
