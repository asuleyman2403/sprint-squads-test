import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuardService as AuthGuard } from './shared/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'users',
    loadChildren: async () =>
      await import('./users/users.module').then((m) => m.UsersModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'repos',
    loadChildren: async () =>
      await import('./repos/repos.module').then((m) => m.ReposModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    redirectTo: 'users',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
