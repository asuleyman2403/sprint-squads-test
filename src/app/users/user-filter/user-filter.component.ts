import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/shared/services/api.service';
import { DataService } from 'src/app/shared/services/data.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-user-filter',
  templateUrl: './user-filter.component.html',
  styleUrls: ['./user-filter.component.scss'],
  providers: [MessageService]
})
export class UserFilterComponent implements OnInit {
  form: FormGroup;
  loading = false;
  constructor(
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private apiService: ApiService,
    private dataService: DataService
  ) {
    this.form = this.formBuilder.group({
      query: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {}

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    const { query } = this.form.value;
    this.loading = true;
    this.apiService
      .searchUsers(query)
      .toPromise()
      .then((data) => {
        const users = data.items;
        this.dataService.updateFilteredUsersSource(users);
        if (users.length) {
          this.messageService.add({
            severity: 'success',
            summary: 'User Search',
            detail: `${users.length} users found`,
          });
        } else {
          this.messageService.add({
            severity: 'warn',
            summary: 'User Search',
            detail: `No user found`,
          });
        }
      })
      .catch((error) =>
        this.messageService.add({
          severity: 'error',
          summary: 'User Search',
          detail: error.message,
        })
      )
      .finally(() => {
        this.loading = false;
        this.form.reset();
      });
  }
}
