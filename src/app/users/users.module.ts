import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UserFilterComponent } from './user-filter/user-filter.component';
import { UsersTableComponent } from './users-table/users-table.component';
import { UsersComponent } from './users/users.component';
import { UsersRoutingModule } from './users-routing.module';
import { CustomPrimeModule } from '../custom-prime.module';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from 'src/app/shared/services/api.service';
import { DataService } from 'src/app/shared/services/data.service';
import { MessageService } from 'primeng/api';

@NgModule({
  declarations: [UserFilterComponent, UsersTableComponent, UsersComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UsersRoutingModule,
    CustomPrimeModule,
    HttpClientModule,
  ],
  providers: [MessageService, ApiService, DataService],
})
export class UsersModule {}
