import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from 'src/app/shared/services/data.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss'],
})
export class UsersTableComponent implements OnInit, OnDestroy {
  users: any = [];
  usersSubscription: Subscription | null = null;
  selectedUsers: any = [];
  constructor(
    private dataService: DataService,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.usersSubscription = this.dataService.filteredUsers$.subscribe(
      (users) => {
        this.users = users;
        this.selectedUsers = [];
      }
    );
  }

  deleteSelectedUsers() {
    this.dataService.updateFilteredUsersSource(
      this.users.filter((user: any) =>
        this.selectedUsers.find(
          (selectedUser: any) => selectedUser.id === user.id
        )
          ? false
          : true
      )
    );

    this.messageService.add({
      severity: 'success',
      summary: 'Selected users has been deleted!',
      detail: 'Message details: none',
    });
  }

  ngOnDestroy() {
    this.usersSubscription?.unsubscribe();
  }

  navigateToUserRepos(user: any) {
    this.router.navigate(['/repos'], {
      queryParams: {
        user: user.login,
      },
    });
  }
}
