import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { format } from 'date-fns';

@Component({
  selector: 'app-repos-table',
  templateUrl: './repos-table.component.html',
  styleUrls: ['./repos-table.component.scss'],
})
export class ReposTableComponent implements OnInit {
  @Input() repos: any = [];
  @Input() language: string = '';
  @Output() deleteRepositories = new EventEmitter<any>();
  selectedRepos: any = [];
  constructor() {}

  ngOnInit(): void {}

  deleteSelectedRepos() {
    this.deleteRepositories.emit(this.selectedRepos);
  }

  getFormattedDate(date: string) {
    return format(new Date(date), 'dd.MM.yyyy');
  }
}
