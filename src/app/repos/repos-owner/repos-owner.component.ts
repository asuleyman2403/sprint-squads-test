import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { format } from 'date-fns';

@Component({
  selector: 'app-repos-owner',
  templateUrl: './repos-owner.component.html',
  styleUrls: ['./repos-owner.component.scss'],
})
export class ReposOwnerComponent implements OnInit {
  @Input() user: any;
  @Input() formVisibility = false;
  @Output() changeVisibility = new EventEmitter();
  formattedDate = '';
  constructor() {}

  ngOnInit(): void {
    this.formattedDate = format(new Date(this.user.created_at), 'dd.MM.yyyy');
  }

  setVisible() {
    this.changeVisibility.emit(true);
  }
}
