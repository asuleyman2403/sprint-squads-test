import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.scss'],
})
export class UserSearchComponent implements OnInit {
  form: FormGroup;
  loading = false;
  @Input() initialValues: any = null;
  @Output() updateUser = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      login: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    if (this.initialValues) {
      this.form.patchValue({ ...this.initialValues });
    }
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    const { login } = this.form.value;
    this.updateUser.emit(login);
  }
}
