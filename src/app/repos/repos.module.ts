import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReposComponent } from './repos/repos.component';
import { ReposTableComponent } from './repos-table/repos-table.component';
import { ReposOwnerComponent } from './repos-owner/repos-owner.component';
import { UserSearchComponent } from './user-search/user-search.component';
import { ReposRoutingModule } from './repos-routing.module';
import { CustomPrimeModule } from 'src/app/custom-prime.module';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from 'src/app/shared/services/api.service';
import { DataService } from 'src/app/shared/services/data.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { RepositoryCreateComponent } from './repository-create/repository-create.component';

@NgModule({
  declarations: [
    ReposComponent,
    ReposTableComponent,
    ReposOwnerComponent,
    UserSearchComponent,
    RepositoryCreateComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ReposRoutingModule,
    HttpClientModule,
    CustomPrimeModule,
  ],
  providers: [ApiService, DataService, MessageService],
})
export class ReposModule {}
