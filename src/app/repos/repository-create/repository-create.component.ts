import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-repository-create',
  templateUrl: './repository-create.component.html',
  styleUrls: ['./repository-create.component.scss'],
})
export class RepositoryCreateComponent implements OnInit {
  form: FormGroup;
  languages: any[] = [];
  @Output() changeVisibility = new EventEmitter();
  @Output() raiseValues = new EventEmitter();
  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      language: [null],
    });
    this.languages = [
      {
        title: 'C++',
        value: 'C++',
      },
      {
        title: 'HTML',
        value: 'HTML',
      },
      {
        title: 'CSS',
        value: 'CSS',
      },
      {
        title: 'JavaScript',
        value: 'JavaScript',
      },
      {
        title: 'TypeScript',
        value: 'TypeScript',
      },
      {
        title: 'Python',
        value: 'Python',
      },
      {
        title: 'Assembly',
        value: 'Assembly',
      },
      {
        title: 'Java',
        value: 'Java',
      },
      {
        title: 'unspecified',
        value: null,
      },
    ];
  }

  ngOnInit(): void {}

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.raiseValues.emit(this.form.value);
  }

  onCancel() {
    this.changeVisibility.emit(false);
  }

  get name() {
    return this.form.get('name');
  }

  get description() {
    return this.form.get('description');
  }

  get language() {
    return this.form.get('language');
  }
}
