import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/shared/services/api.service';
import { DataService } from 'src/app/shared/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { v4 as uuv4 } from 'uuid';
@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: ['./repos.component.scss'],
})
export class ReposComponent implements OnInit, OnDestroy {
  currentUser: any = null;
  repositories: any[] = [];
  hintMessages: any[] = [];
  reposByLanguage: any[] = [];
  formInitialValues = {
    login: '',
  };
  currenUserSubscription: Subscription | null = null;
  repositoriesSubscription: Subscription | null = null;
  routerSubscription: Subscription | null = null;
  formVisibility = false;
  constructor(
    private dataService: DataService,
    private apiService: ApiService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.currenUserSubscription = this.dataService.currentUser$.subscribe(
      (user) => {
        this.currentUser = user;
        this.repositories = [];
        if (this.currentUser) {
          this.formInitialValues = { login: user.login };
          if (!this.checkLocalStorage(this.currentUser.login)) {
            this.getRepositories();
          }
        }
      }
    );

    this.repositoriesSubscription = this.dataService.repositories$.subscribe(
      (reps) => {
        this.repositories = reps;
        if (this.repositories && this.currentUser) {
          this.saveRepositoriesLocally();
          this.getReposByLanguage();
        }
      }
    );

    this.routerSubscription = this.route.queryParams.subscribe((params) => {
      const { user } = params;
      if (user) {
        const userWithReps = this.checkLocalStorage(user);
        if (!userWithReps) {
          this.fetchUser(user);
        } else {
          this.updateFromLocalStorage(userWithReps);
        }
      } else {
        this.hintMessages = [
          {
            severity: 'info',
            summary: 'Info',
            detail: 'Find user first to see repositories',
          },
        ];
      }
    });
  }

  ngOnDestroy(): void {
    this.currenUserSubscription?.unsubscribe();
    this.repositoriesSubscription?.unsubscribe();
    this.routerSubscription?.unsubscribe();
  }

  changeVisibility($event: any) {
    this.formVisibility = $event;
  }

  saveRepositoriesLocally() {
    const repsWithOwners = JSON.parse(
      localStorage.getItem('repsWithOwners') as any
    ) as any[];
    if (repsWithOwners && repsWithOwners.length) {
      if (
        repsWithOwners.find((r) => r.owner.login === this.currentUser.login)
      ) {
        const updatedRepsWithOwner = repsWithOwners.map((r) => {
          if (r.owner.login === this.currentUser.login) {
            return {
              owner: this.currentUser,
              repos: this.repositories,
            };
          }
          return r;
        });
        localStorage.setItem(
          'repsWithOwners',
          JSON.stringify(updatedRepsWithOwner)
        );
      } else {
        localStorage.setItem(
          'repsWithOwners',
          JSON.stringify([
            ...repsWithOwners,
            {
              owner: this.currentUser,
              repos: this.repositories,
            },
          ])
        );
      }
    } else {
      localStorage.setItem(
        'repsWithOwners',
        JSON.stringify([
          {
            owner: this.currentUser,
            repos: this.repositories,
          },
        ])
      );
    }
  }

  checkLocalStorage(login: string) {
    const data = localStorage.getItem('repsWithOwners');
    if (data) {
      const repsWithOwners = JSON.parse(data) as any[];
      return repsWithOwners.find((r: any) => r.owner?.login === login);
    }
    return null;
  }

  updateFromLocalStorage(repsWithOwner: any) {
    const { owner, repos } = repsWithOwner;
    this.dataService.updateCurrentUser(owner);
    this.dataService.updateRepositories(repos);
    this.messageService.add({
      severity: 'success',
      summary: 'Found',
      detail: `Found from previously saved session!`,
    });
  }

  getReposByLanguage() {
    const repos: any = {
      unspecified: [],
    };
    this.repositories.forEach((item, index) => {
      const { language } = item;
      if (language) {
        if (repos[language] && Array.isArray(repos[language])) {
          repos[language].push(item);
        } else {
          repos[language] = [item];
        }
      } else {
        repos['unspecified'].push(item);
      }
    });
    const tranformedRepos = Object.keys(repos).map((key) => ({
      language: key,
      repos: repos[key],
    }));
    this.reposByLanguage = tranformedRepos;
  }

  updateCurrentUser($event: any) {
    const login = $event;
    this.router.navigate(['/repos'], {
      queryParams: {
        user: login,
      },
    });
  }

  fetchUser(login: string) {
    this.apiService
      .getExactUser(login)
      .toPromise()
      .then((user) => {
        this.dataService.updateCurrentUser(user);
        this.messageService.add({
          severity: 'success',
          summary: 'GET: User',
          detail: `User ${login} has been found!`,
        });
      })
      .catch((error) =>
        this.messageService.add({
          severity: 'error',
          summary: 'GET: USER',
          detail: error.message,
        })
      )
      .finally(() => {});
  }

  getRepositories() {
    this.apiService
      .getUserRepositories(this.currentUser.login)
      .toPromise()
      .then((reps: any) => {
        this.dataService.updateRepositories(reps);
        this.messageService.add({
          severity: 'success',
          summary: 'GET: Repositories',
          detail: `${reps.length} public repositories found!`,
        });
      })
      .catch((error) => {
        this.messageService.add({
          severity: 'error',
          summary: 'GET: Repositories',
          detail: error.message,
        });
      });
  }

  deleteRepositories(selectedRepos: any) {
    this.dataService.updateRepositories(
      this.repositories.filter((r) =>
        selectedRepos.find((s: any) => s.id === r.id) ? false : true
      )
    );
  }

  createNewRepository(formValue: any) {
    const { name, description, language } = formValue;
    const repository = {
      name,
      description,
      language: language.value,
      created_at: new Date().toISOString(),
      url: 'javascript:void(0)',
      id: uuv4(),
    };
    this.dataService.updateRepositories([...this.repositories, repository]);
    this.formVisibility = false;
  }
}
